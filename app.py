from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String

SQLALCHEMY_DATABASE_URL = "mysql+pymysql://root:1234@localhost:3306/lunch"

engine= create_engine(
    SQLALCHEMY_DATABASE_URL
)

SessionLocal= sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
session= SessionLocal()

class Meal(Base):
    __tablename__= 'meals'
    id= Column(Integer, primary_key=True, autoincrement=True)  
    sandwich = Column(String(20), nullable=False)
    fruit = Column(String(20), nullable=False)
    tablenumber= Column(Integer)

    def __repr__(self):
        return '[{}, {}, {},{}]'.format(self.id, self.sandwich, self.fruit, self.tablenumber)


Base.metadata.create_all(bind=engine)



meal = Meal(id=1, sandwich='Libanais', fruit='banane', tablenumber= 21)
print('----->', meal)
session.add(meal)
session.commit()